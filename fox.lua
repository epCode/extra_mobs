--MCmobs v0.4
--maikerumine
--made for MC like Survival game
--License for code WTFPL and otherwise stated in readmes

local S = minetest.get_translator("extra_mobs")

--###################
--################### fox
--###################

local followitem = ""
if minetest.get_modpath("mc_sweet_berry") then
    followitem = "mc_sweet_berry:sweet_berry"
else
    followitem = nil
end

local fox = {
	type = "monster",
	passive = false,
	spawn_class = "hostile",
	hp_min = 10,
	hp_max = 10,
	xp_min = 1,
	xp_max = 2,
	rotate = -90,
	armor = {fleshy = 90},
	attack_type = "dogfight",
	damage = 2,
	reach = 1.5,
	collisionbox = {-0.3, -0.01, -0.3, 0.3, 0.84, 0.3},
	visual = "mesh",
	mesh = "extra_mobs_fox.b3d",
	textures = { {
		"extra_mobs_fox.png",
		"extra_mobs_trans.png",
	} },
	visual_size = {x=3, y=3},
	sounds = {
	},
	jump = true,
	makes_footstep_sound = true,
	walk_velocity = 3,
	run_velocity = 6,
	drops = {
	},
	animation = {
		stand_speed = 7,
		walk_speed = 17,
		run_speed = 15,
		stand_start = 11,
		stand_end = 11,
		walk_start = 0,
		walk_end = 10,
		run_start = 0,
		run_end = 10,
		pounce_start = 11,
		pounce_end = 31,
		lay_start = 34,
		lay_end = 34,
	},
	skittish = true,
	on_spawn = function(self)
		if minetest.find_node_near(self.object:get_pos(), 4, "mcl_core:snow") ~= nil or minetest.find_node_near(self.object:get_pos(), 4, "mcl_core:dirt_with_grass_snow") ~= nil then
			self.object:set_properties({textures={"extra_mobs_artic_fox.png", "extra_mobs_trans.png"}})
		end
	end,
	follow = followitem,
	fear_height = 4,
	view_range = 16,
	specific_attack = { "mobs_mc:chicken", "extra_mobs:cod", "extra_mobs:salmon" },
}

mobs:register_mob("extra_mobs:fox", fox)

-- spawning
mobs:spawn_specific(
"extra_mobs:fox",
"overworld",
"ground",
{
	"FlowerForest_beach",
	"Forest_beach",
	"StoneBeach",
	"ColdTaiga_beach_water",
	"Taiga_beach",
	"Savanna_beach",
	"Plains_beach",
	"ExtremeHills_beach",
	"ColdTaiga_beach",
	"Swampland_shore",
	"JungleM_shore",
	"Jungle_shore",
	"MesaPlateauFM_sandlevel",
	"MesaPlateauF_sandlevel",
	"MesaBryce_sandlevel",
	"Mesa_sandlevel",
	"Mesa",
	"FlowerForest",
	"Swampland",
	"Taiga",
	"ExtremeHills",
	"Jungle",
	"Savanna",
	"BirchForest",
	"MegaSpruceTaiga",
	"MegaTaiga",
	"ExtremeHills+",
	"Forest",
	"Plains",
	"Desert",
	"ColdTaiga",
	"IcePlainsSpikes",
	"SunflowerPlains",
	"IcePlains",
	"RoofedForest",
	"ExtremeHills+_snowtop",
	"MesaPlateauFM_grasstop",
	"JungleEdgeM",
	"ExtremeHillsM",
	"JungleM",
	"BirchForestM",
	"MesaPlateauF",
	"MesaPlateauFM",
	"MesaPlateauF_grasstop",
	"MesaBryce",
	"JungleEdge",
	"SavannaM",
},
0,
minetest.LIGHT_MAX+1,
30,
15000,
3,
mobs_mc.spawn_height.overworld_min,
mobs_mc.spawn_height.overworld_max)

-- spawn eggs
mobs:register_egg("extra_mobs:fox", S("Fox"), "extra_mobs_spawn_icon_fox.png", 0)
